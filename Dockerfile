FROM nixos/nix:latest
MAINTAINER K Gopal Krishna <mail@kayg.org>

RUN nix-channel --add https://nixos.org/channels/nixos-20.09 nixos
RUN nix-channel --update
RUN nix-env -iA nixos.git

# Garbage collect after installing
RUN nix-collect-garbage
